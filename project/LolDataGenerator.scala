import scala.util.matching.Regex
// import sys.process._
import java.net.{URL, HttpURLConnection}
import java.io._
// import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD

object LolDataGenerator {
    val conf = new SparkConf().setAppName("LolDataGenerator").setMaster("local[2]").set("spark.executor.memory","1g");
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val spark = SparkSession.builder().getOrCreate()
    import sqlContext.implicits._

    val apiUrlPrefix = "https://br1.api.riotgames.com"
    val apiKeySuffix = "?api_key=RGAPI-41a4c27c-2d0c-4424-b98a-59e464659a9b" //"?api_key=RGAPI-f08f49ff-27fb-4389-b413-fe17dc9d8370"
    val jsonExtension = ".json"

    val supportingFilesDirectory = "supportingFiles/"
    val playersDirectory = supportingFilesDirectory + "players/"
    val accountIdsDirectory = supportingFilesDirectory + "accountIds/"
    val gamesDirectory = supportingFilesDirectory + "games/"

    def main(args: Array[String]) {
        sc.setLogLevel("ERROR")

        val matchApiUrl = "/lol/match/v3/matches/"
        val gameIds = getGameIdsFromChallengerLeague()

        // val writer = new PrintWriter(new File(supportingFilesDirectory + "results.txt"))

        // var count = 0
        // getListOfGameFiles(gamesDirectory).foreach { file =>
        //     if (file.exists() && (file.length() > 0)) {
        //         val gameDf = matchDataFrameFrom(gamesDirectory + file.getName())
        //         println(s"Escrevendo linha ${count}")
        //         writer.write(generateResultsFrom(gameDf))
        //         writer.write("\n")
        //         count += 1
        //     }
        // }

        // writer.close()

        getResults(getAllChampIds())
        sc.stop()
    }

    def getAllChampIds(): Array[Long] = {

        // CAMPEOES
        println("Gerando array de campeoes...")
        val championsJson = supportingFilesDirectory + "champions.json"
        val championsDf = championsDataFrameFrom(championsJson)
        return championsDf.map(_.getAs[Long]("id")).collect
    }

    def generateResultsFrom(gameDf: DataFrame): String = {

        // (key: (gameId, win), value: championId).groupByKey() = (key: (gameId, win), value: championIds array)
        val gamesRdd = gameDf.rdd.map(row => ((row(3), row(2)), row(1))).groupByKey()

        // (key: (gameId, win), value: calculated team id)
        val gamesRdd2 = gamesRdd.map(kv => (kv._1, idFromTeamSeq(kv._2.asInstanceOf[Seq[Long]])))

        // (key: gameId, value: (calculated team id, win))
        val gamesRdd3 = gamesRdd2.map(kv => (kv._1._1, (kv._2, kv._1._2)))

        val gamesRdd4 = gamesRdd3.groupByKey()

        // (key: (teamId1, teamId2), value: 1)
        // teamId1 perdeu pro teamId2 uma vez
        val gamesRdd5 = gamesRdd4.map(kv => {
                val tupleSeq = kv._2.asInstanceOf[Seq[(Long, String)]]
                val teamIdAndWin1 = tupleSeq(0)
                val teamIdAndWin2 = tupleSeq(1)

                if (teamIdAndWin1._2 == "Win") {
                    ((teamIdAndWin2._1, teamIdAndWin1._1), 1)
                } else {
                    ((teamIdAndWin1._1, teamIdAndWin2._1), 1)
                }
            })

        // ---- Gambiarra ----
        // Esse RDD so tem uma linha, e eu retorno ela em csv
        gamesRdd5.take(1).foreach { row =>
            return row._1._1 + "," + row._1._2 + "," + row._2
        }

        return ""
        // -------------------
    }

    def getResults(adversaryArray: Array[Long]) {
        println("Calculando resultados...")

        // (teamId1, teamId2, 1)
        // teamId1 perdeu pro teamId2 1 vez
        val dataFileName = supportingFilesDirectory + "results.txt" // "/var/www/html/projeto-bigdata/project/data.txt"
        val dataFile = sc.textFile(dataFileName)
        val dataRdd = dataFile.map { line =>
            val a = line.split(",")
            (a(0).toLong, a(1).toLong, a(2).toLong)
        }

        val winnerChampionsRdd = dataRdd.flatMap { case (teamId1, teamId2, n) =>
            val team1Array = teamArrayFromId(teamId1)
            val team2Array = teamArrayFromId(teamId2)

            // (key: (campId1, campId2), value: 1)
            // campId1 perdeu pro campId2 1 vez
            var championsArray = Array[((Long, Long), Long)]()
            team1Array.foreach { champId1 =>
                team2Array.foreach { champId2 =>
                    championsArray :+= ((champId1, champId2), 1L)
                }
            }

            championsArray
        }

        val totalChampionsRdd = dataRdd.flatMap { case (teamId1, teamId2, n) =>
            val team1Array = teamArrayFromId(teamId1)
            val team2Array = teamArrayFromId(teamId2)

            // (key: (campId1, campId2), value: 1)
            // campId1 jogou com o campId2 1 vez
            var championsArray = Array[((Long, Long), Long)]()
            team1Array.foreach { champId1 =>
                team2Array.foreach { champId2 =>
                    championsArray :+= ((champId1, champId2), 1L)
                    championsArray :+= ((champId2, champId1), 1L)
                }
            }

            championsArray
        }

        // (key: (campId1, campId2), value: N)
        // campId1 perdeu pro campId2 N vezes
        val winnerChampionsRdd2 = winnerChampionsRdd.reduceByKey((v1, v2) => v1 + v2)

        // (key: (campId1, campId2), value: N)
        // campId1 jogou com o campId2 N vezes
        val totalChampionsRdd2 = totalChampionsRdd.reduceByKey((v1, v2) => v1 + v2)

        val writer = new PrintWriter(new File(supportingFilesDirectory + "data.txt"))

        var count1 = 1
        adversaryArray.foreach { advChampId =>
            println(s"Adversario ${count1}, id=${advChampId}...")
            count1 += 1
            val winnerSuggestions = winnerChampionsRdd2.filter { case (champs, n) => champs._1 == advChampId }
            val totalSuggestions = totalChampionsRdd2.filter { case (champs, n) => champs._1 == advChampId }

            // (percentage, (champSuggestionId, n, total))
            // champSuggestionId ganhou de advChampId percentage% das vezes (n/total)
            winnerSuggestions.collect.foreach { case (winnerChamps, n) =>
                val total = totalSuggestions.filter { case (totalChamps, m) => totalChamps == winnerChamps }.first()._2
                val percentage = 100*n.toFloat/total.toFloat
                writer.write(s"${advChampId},${winnerChamps._2},${"%1.2f".format(percentage)},${n},${total}")
                writer.write("\n")
            }
        }

        writer.close()

    }

    def getGameIdsFromChallengerLeague(): RDD[String] = {
      // val accountIds = sc.parallelize(Array("10", "20", "30"))
      // def test(x: String): RDD[String] = {
      //   val rdd1 = sc.parallelize(Array("1", "2"))
      //   val rdd2 = sc.parallelize(Array("4", "3"))
      //   val rdd3 = sc.parallelize(Array("6", "2"))
      //
      //   if (x == "10") rdd1 else if (x == "20") rdd2 else rdd3
      // }
      // return accountIds.collect.toList.foldLeft(sc.emptyRDD[String]){(z,i) => z.union(test(i))}.distinct

      val accountIds = getAccountIdsFromChallengerLeague()

      return accountIds.collect.toList.foldLeft(sc.emptyRDD[String]){(z,i) => z.union(getGameIdsFrom(i))}.distinct
    }

    def getGameIdsFrom(accountId: String): RDD[String] = {
      val accountUrl = s"/lol/match/v3/matchlists/by-account/${accountId}"
      val accountFileName = accountIdsDirectory + accountId + jsonExtension

      downloadApiFileFrom(accountUrl, accountFileName)
      val df = spark.read.json(accountFileName)

      return df.select(explode(df("matches"))).toDF("games").select("games.gameId").map(_.getAs[Long]("gameId").toString).rdd.distinct
    }

    // gera um RDD com os accountIds de todos jogadores da challenger league
    def getAccountIdsFromChallengerLeague(): RDD[String] = {
        val challengerLeagueDF = getChallengerLeagueDF()
        val playerNamesDF = challengerLeagueDF.select(explode(challengerLeagueDF("entries"))).toDF("players").select("players.playerOrTeamName")

        val pattern = " ".r
        return playerNamesDF.map(row => getAccountIdFrom(pattern.replaceAllIn(row.getAs[String]("playerOrTeamName"), "%20"))).rdd
    }

    // pega o accountId a partir do nome do jogador
    def getAccountIdFrom(playerName: String): String = {
        val playerUrl = "/lol/summoner/v3/summoners/by-name/" + playerName
        val playerFileName = playersDirectory + playerName + jsonExtension

        downloadApiFileFrom(playerUrl, playerFileName)

        return spark.read.json(playerFileName).first().getAs[Long]("accountId").toString
    }

    // baixa e gera o DataFrame de challenger league
    def getChallengerLeagueDF(): DataFrame = {
        val challengerLeagueUrl = "/lol/league/v3/challengerleagues/by-queue/RANKED_SOLO_5x5"
        val challengerleagueFileName = supportingFilesDirectory + "challengerLeague" + jsonExtension

        downloadApiFileFrom(challengerLeagueUrl, challengerleagueFileName)

        return spark.read.json(challengerleagueFileName)
    }

    // baixa um arquivo da API atraves da URL especifica desse request
    def downloadApiFileFrom(apiUrl: String, fileName: String) = {
        val file = new File(fileName)

        if (!file.exists()) {
            print(s"Baixando da API arquivo ${fileName} ... ")
            Thread.sleep(1250)
            downloadUrlToFile(apiUrlPrefix + apiUrl + apiKeySuffix, fileName)
            println("Feito!")
        }
    }

    // baixa o arquivo de uma URL pra um arquivo local
    def downloadUrlToFile(url: String, fileName: String) = {
        // http://alvinalexander.com/scala/scala-how-to-download-url-contents-to-string-file
        // new URL(url) #> new File(fileName) !! // nao funcionou quando a API retorna 404

        val file = new File(fileName)

        if (!file.exists()) {
            val fileUrl = new URL(url)
            val connection = fileUrl.openConnection().asInstanceOf[HttpURLConnection]
            connection.setRequestMethod("GET")

            var in: InputStream = null
            var error = false

            try {
                in = connection.getInputStream
            } catch {
                case ex: FileNotFoundException => {
                    error = true
                    println("\nArquivo nao encontrado na URL " + url + "\n")
                }
                case ex: IOException => {
                    error = true
                    println("\n" + ex.toString + "\n")
                }
            }

            if (!error) {
                val out: OutputStream = new BufferedOutputStream(new FileOutputStream(file))
                val byteArray = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray
                out.write(byteArray)
                out.close()
                in.close()
            } else {
                file.createNewFile(); // cria um arquivo vazio
            }
        }
    }

    // gera o DataFrame de uma partida a partir de seu json
    def matchDataFrameFrom(fileName: String): DataFrame = {
        // http://bigdatums.net/2016/02/12/how-to-extract-nested-json-data-in-spark/

        val matchDf = spark.read.json(fileName)
        var teamsDf = matchDf.select(explode(matchDf("teams"))).toDF("teams")
        var participantsDf = matchDf.select(explode(matchDf("participants"))).toDF("participants")

        teamsDf = teamsDf.select("teams.teamId", "teams.win")
        participantsDf = participantsDf.select("participants.teamId", "participants.championId")

        val gameId = matchDf.select("gameId").take(1)(0).getLong(0)
        return participantsDf.join(teamsDf, "teamId").withColumn("gameId", lit(gameId))
    }

    // gera um id unico para cada combinacao de 5 campeoes
    // ex: (19, 51, 91, 161, 267) -> 19051091161267
    def idFromTeamSeq(team: Seq[Long]): Long = {
        return team.sorted.reduceLeft((x, y) => (x * 1000) + y)
    }

    def getListOfGameFiles(dir: String): List[File] = {
        val extensions = List(jsonExtension)
        val d = new File(dir)
        if (d.exists && d.isDirectory) {
            d.listFiles.filter(_.isFile).toList.filter { file =>
                extensions.exists(file.getName.endsWith(_))
            }
        } else {
            List[File]()
        }
    }

    // desconverte a lista de 5 campeoes a partir do id unico gerado acima
    // ex: 19051091161267 -> (19, 51, 91, 161, 267)
    // (deve ter uma maneira menos feia de implementar)
    def teamArrayFromId(id: Long): Array[Long] = {
        val team = new Array[Long](5)

        team(0) = id/1000000000000L
        team(1) = id/1000000000L    - team(0)*1000L
        team(2) = id/1000000L       - team(0)*1000000L       - team(1)*1000L
        team(3) = id/1000L          - team(0)*1000000000L    - team(1)*1000000L    - team(2)*1000L
        team(4) = id/1L             - team(0)*1000000000000L - team(1)*1000000000L - team(2)*1000000L - team(3)*1000L

        return team
    }

    // gera o DataFrame dos campeoes a partir do json
    def championsDataFrameFrom(fileName: String): DataFrame = {
        var championsString = sc.textFile(fileName).first()
        val prefixIndex = championsString.indexOfSlice("\"data\":{")

        championsString = championsString.drop(prefixIndex + 8) // remove ..."data"...
        championsString = championsString.dropRight(2) // remove }}

        val pattern = ",?\"(\\w)+\":[{]".r // ,"string":{
        championsString = pattern.replaceAllIn(championsString, "\n{")
        championsString = championsString.drop(1) // remove primeiro \n

        val championsList = championsString.split("\n")
        val championsRdd = sc.parallelize(championsList)

        return spark.read.json(championsRdd)
    }

}
