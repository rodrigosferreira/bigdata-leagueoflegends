import scala.util.matching.Regex
import sys.process._
import java.net.URL
import java.io._
import java.net.HttpURLConnection
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object LolApp {
    val conf = new SparkConf().setAppName("LolApp")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._

    val spark = SparkSession.builder().getOrCreate()

    def main(args: Array[String]) {
        val inputFileName = args(0)
        val adversaryId = teamIdFromInputFile(inputFileName)

        getResults(adversaryId)
        sc.stop()
    }

    def teamIdFromInputFile(fileName: String): Long = {
        var championsString = sc.textFile(fileName).first()

        championsString = championsString.drop(2) // remove ["
        championsString = championsString.dropRight(2) // remove "]

        val championsList = championsString.split("\",\"")

        val teamId = idFromTeamSeq(championsList.map(_.toLong).toSeq)

        return teamId
    }

    def getResults(adversaryId: Long) {

        // CAMPEOES
        val championsJson = "/var/www/html/projeto-bigdata/project/champions.json"
        val championsDf = championsDataFrameFrom(championsJson)
        println("\n- CAMPEOES")
        championsDf.orderBy(asc("id")).show()

        // LISTA DE JOGOS
        val featuredGamesJson = "/var/www/html/projeto-bigdata/project/featuredGames.json"
        val gamesDf = featuredGamesDataFrameFrom(featuredGamesJson) // <------------------- basta mudar o gamesDf pra nova lista com milhoes de partidas
        println("\n- LISTA DE JOGOS")
        gamesDf.show()

        val gamesDirectory = "/var/www/html/projeto-bigdata/project/games/"
        val jsonExtension = ".json"
        val matchApiUrl = "https://br1.api.riotgames.com/lol/match/v3/matches/"
        val apiKeySuffix = "?api_key=RGAPI-f08f49ff-27fb-4389-b413-fe17dc9d8370"
        val gameIdsRdd = gamesDf.rdd.map(row => row(0).asInstanceOf[Long].toString)
        val fileNameAndUrlsRdd = gameIdsRdd.map(id => (gamesDirectory + id + jsonExtension, matchApiUrl + id + apiKeySuffix))

        fileNameAndUrlsRdd.foreach(fileNameAndUrl => downloadJsonToFile(fileNameAndUrl._2, fileNameAndUrl._1))

        val fileNamesRdd = fileNameAndUrlsRdd.map(fileNameAndUrl => fileNameAndUrl._1).filter(fileName => {
                val file = new File(fileName)
                file.exists() && (file.length() > 0)
            })

        val gamesDfArray = fileNamesRdd.collect.map(fileName => matchDataFrameFrom(fileName))
        val gamesDf2 = gamesDfArray.reduce((df1, df2) => df1.unionAll(df2))

        // (key: (gameId, win), value: championId).groupByKey() = (key: (gameId, win), value: championIds array)
        val gamesRdd = gamesDf2.rdd.map(row => ((row(3), row(2)), row(1))).groupByKey()

        // (key: (gameId, win), value: calculated team id)
        val gamesRdd2 = gamesRdd.map(kv => (kv._1, idFromTeamSeq(kv._2.asInstanceOf[Seq[Long]])))

        // (key: gameId, value: (calculated team id, win))
        val gamesRdd3 = gamesRdd2.map(kv => (kv._1._1, (kv._2, kv._1._2)))

        val gamesRdd4 = gamesRdd3.groupByKey()

        // (key: (teamId1, teamId2), value: 1)
        // teamId1 perdeu pro teamId2 uma vez
        val gamesRdd5 = gamesRdd4.map(kv => {
                val tupleSeq = kv._2.asInstanceOf[Seq[(Long, String)]]
                val teamIdAndWin1 = tupleSeq(0)
                val teamIdAndWin2 = tupleSeq(1)

                if (teamIdAndWin1._2 == "Win") {
                    ((teamIdAndWin2._1, teamIdAndWin1._1), 1)
                } else {
                    ((teamIdAndWin1._1, teamIdAndWin2._1), 1)
                }
            })


        // ---------------------------------- APAGAR ESTE BLOCO ---------------------------------------
        // serve pra adicionar alguns times repetidos manualmente so pra testar o reduceByKey de baixo

        val manuallyAddedGames: Array[((Long, Long), Int)] = new Array[((Long, Long), Int)](5)
        manuallyAddedGames(0) = ((19051091161267L,22044105120420L),1)
        manuallyAddedGames(1) = ((19051091161267L,22044105120420L),1)
        manuallyAddedGames(2) = ((8033068117202L,3163236240412L),1)
        manuallyAddedGames(3) = ((19051091161267L,6043079157236L),1)
        manuallyAddedGames(4) = ((19051091161267L,6043079157236L),1)
        val manuallyAddedGamesRdd: org.apache.spark.rdd.RDD[((Long, Long), Int)] = sc.parallelize(manuallyAddedGames)
        val gamesRdd6 = gamesRdd5.union(manuallyAddedGamesRdd)

        // --------------------------------------------------------------------------------------------


        // (key: (teamId1, teamId2), value: N)
        // teamId1 perdeu pro teamId2 N vezes
        val gamesRdd7 = gamesRdd6.reduceByKey((v1, v2) => v1 + v2)

        // time que vai ser recebido como entrada da web
        // (precisa converter a array de championsIds recebida para um teamId usando a funcao idFromTeam)
        val adversaryInputExample = 19051091161267L

        // (key: N, value: (adversaryInputExampleId, teamSuggestionId)), ordenado por N
        // adversaryInputExampleId perdeu pro teamSuggestionId N vezes
        val teamSuggestionsRdd = gamesRdd7.filter(kv => kv._1._1 == adversaryInputExample).map(kv => (kv._2, kv._1)).sortByKey(false)
        println("\n- LISTA DE SUGESTOES")
        teamSuggestionsRdd.collect.foreach(println)

        // (teamSuggestionId, N)
        // aqui era bom ter mantido o numero de vitorias N, mas nao adiantou pq deu problema no debaixo (nao consegui gerar um rdd de arrays, so array de arrays)
        val teamSuggestionsRdd2 = teamSuggestionsRdd.map { case (n, ids) => (ids._2, n) }

        // (Array[Array[String]], N) = (array de arrays de time, N)
        // tentei fazer rdd mas sla pq essa bosta ficou dando NullPointerException na hora de acessar o rdd
        // era bom ser rdd pra poder deixar o numero de vitorias N dentro da tupla tbm, pra poder imprimir junto com o time
        val teamSuggestionsArray = teamSuggestionsRdd2.collect.map { case (sugId, n) => (championNamesFromTeamId(sugId), n) }

        println("\n- TIME ADVERSARIO:\n")
        championNamesFromTeamId(adversaryInputExample).foreach(println)
        println("\n- SUAS OPCOES DE TIME:\n")
        teamSuggestionsArray.foreach { case (array, n) => {
                println(s"Esta combinacao ganhou ${n} vezes de seu adversario:")
                array.foreach(println)
                println()
            }}



        val writer = new PrintWriter(new File("/var/www/html/projeto-bigdata/storage/app/results.txt"))

        writer.write(s"- TIME DA ENTRADA (ID ${adversaryId}):\n\n")
        championNamesFromTeamId(adversaryId).foreach { name =>
            writer.write(name)
            writer.write("\n")
        }

        writer.write("\n- TIME ADVERSARIO:\n\n")
        championNamesFromTeamId(adversaryInputExample).foreach { name =>
            writer.write(name)
            writer.write("\n")
        }
        writer.write("\n- SUAS OPCOES DE TIME:\n\n")
        teamSuggestionsArray.foreach { case (array, n) => {
                writer.write(s"Esta combinacao ganhou ${n} vezes de seu adversario:\n")
                array.foreach { name =>
                    writer.write(name)
                    writer.write("\n")
                }
                writer.write("\n")
            }}
        writer.close()

    }

    // gera o DataFrame dos campeoes a partir do json
    def championsDataFrameFrom(fileName: String): DataFrame = {
        var championsString = sc.textFile(fileName).first()
        val prefixIndex = championsString.indexOfSlice("\"data\":{")

        championsString = championsString.drop(prefixIndex + 8) // remove ..."data"...
        championsString = championsString.dropRight(2) // remove }}

        val pattern = ",?\"(\\w)+\":[{]".r // ,"string":{
        championsString = pattern.replaceAllIn(championsString, "\n{")
        championsString = championsString.drop(1) // remove primeiro \n

        val championsList = championsString.split("\n")
        val championsRdd = sc.parallelize(championsList)

        return spark.read.json(championsRdd)
    }

    // gera o DataFrame de uma partida a partir de seu json
    def matchDataFrameFrom(fileName: String): DataFrame = {
        // http://bigdatums.net/2016/02/12/how-to-extract-nested-json-data-in-spark/

        val matchDf = spark.read.json(fileName)
        var teamsDf = matchDf.select(explode(matchDf("teams"))).toDF("teams")
        var participantsDf = matchDf.select(explode(matchDf("participants"))).toDF("participants")

        teamsDf = teamsDf.select("teams.teamId", "teams.win")
        participantsDf = participantsDf.select("participants.teamId", "participants.championId")

        val gameId = matchDf.select("gameId").take(1)(0).getLong(0)
        return participantsDf.join(teamsDf, "teamId").withColumn("gameId", lit(gameId))
    }

    // gera uma lista de partidas a partir de um json de featuredGames
    def featuredGamesDataFrameFrom(fileName: String): DataFrame = {
        var gamesDf = spark.read.json(fileName)

        gamesDf = gamesDf.select(explode(gamesDf("gameList"))).toDF("gameList")
        gamesDf = gamesDf.select("gameList.gameId")

        return gamesDf
    }

    // baixa um json de uma url pra um arquivo
    def downloadJsonToFile(url: String, fileName: String) = {
        // http://alvinalexander.com/scala/scala-how-to-download-url-contents-to-string-file
        // new URL(url) #> new File(fileName) !! // nao funcionou quando a API retorna 404

        val file = new java.io.File(fileName)

        if (!file.exists()) {
            try {
                val fileUrl = new URL(url)
                val connection = fileUrl.openConnection().asInstanceOf[HttpURLConnection]
                connection.setRequestMethod("GET")

                val in: InputStream = connection.getInputStream

                val out: OutputStream = new BufferedOutputStream(new FileOutputStream(file))
                val byteArray = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray

                out.write(byteArray)
                in.close()
                out.close()
            } catch {
                case ex: java.io.FileNotFoundException => {
                    file.createNewFile(); // cria um arquivo vazio
                    println("\nArquivo nao encontrado na URL " + url + "\n")
                }
            }
        }

    }

    // gera um DataFrame com o nome de cada campeao de um jogo e seu status de vitoria
    // (nao ta sendo usada)
    def championResultsDataFrameFrom(matchDf: DataFrame, championsDf: DataFrame): DataFrame = {
        val joinedDf = matchDf.as('match).join(championsDf.as('champions), $"match.championId" === $"champions.id")
        return joinedDf.select($"name", $"win")
    }

    // gera um id unico para cada combinacao de 5 campeoes
    // ex: (19, 51, 91, 161, 267) -> 19051091161267
    def idFromTeamSeq(team: Seq[Long]): Long = {
        return team.sorted.reduceLeft((x, y) => (x * 1000) + y)
    }

    // desconverte a lista de 5 campeoes a partir do id unico gerado acima
    // ex: 19051091161267 -> (19, 51, 91, 161, 267)
    // (deve ter uma maneira menos feia de implementar)
    def teamArrayFromId(id: Long): Array[Long] = {
        val team = new Array[Long](5)

        team(0) = id/1000000000000L
        team(1) = id/1000000000L    - team(0)*1000L
        team(2) = id/1000000L       - team(0)*1000000L       - team(1)*1000L
        team(3) = id/1000L          - team(0)*1000000000L    - team(1)*1000000L    - team(2)*1000L
        team(4) = id/1L             - team(0)*1000000000000L - team(1)*1000000000L - team(2)*1000000L - team(3)*1000L

        return team
    }

    // gera uma array de nomes de campeoes baseado num id de time
    def championNamesFromTeamId(id: Long): Array[String] = {
        val championsJson = "/var/www/html/projeto-bigdata/project/champions.json"
        val championsDf = championsDataFrameFrom(championsJson)

        val championIdsArray = teamArrayFromId(id)
        val nameArray = championIdsArray.map(championId => {
            val nameDf = championsDf.select("name").where("id == " + championId.toString)
            val name = nameDf.first()(0).toString
            name
        })

        return nameArray
    }

}

/*

chegar num rdd final que seja tipo (teamId1, teamId2, N) (nao necessariamente nessa ordem)
o que significa que o time teamId1 perdeu pro time teamId2 N vezes (é parecido com o word count do professor)

assim, a gente vai receber da web 5 campeoes, vai gerar o id unico dessa combinacao, e vai olhar as linhas que tem o teamId1 igual a esse id

Ai, basta escolher o teamId2 da linha que tiver o maior N (esse vai ser o melhor time contra o teamId1)

exemplo de como vai ta no rdd no final de tudo:

(time1, time2, 670)
(time1, time3, 332)
(time1, time4, 54)
(time2, time5, 652)
(time2, time6, 353)
(time2, time7, 111)
...

ou seja,
se o time adversario for o time1, a gente escolhe o time2, pq ele ganhou 670 vezes
se o time adversario for o time2, a gente escolhe o time5, pq ele ganhou 652 vezes
etc

onde parou:

fiz praticamente tudo isso, o que resultou num rdd (teamSuggestionsRdd) assim: (N, (adversaryInputExample, teamSuggestion))

exemplo que tá rodando:
(3,(19051091161267,22044105120420))
(2,(19051091161267,6043079157236))

ou seja,
o time 22044105120420 ganhou 3 vezes do time 19051091161267
o time 6043079157236 ganhou 2 vezes do time 19051091161267

depois converto esses IDs em arrays de nomes de campeoes e imprimo as sugestoes

agora, faltam 3 coisas:

1- mudar a lista de partidas que é lida. Por enquanto eu so pego as partidas do arquivo featuredGames, e adiciono algumas
    manualmente so pra repetir alguns times e aparecer no exemplo (depois tem q tirar o bloco q faz isso).
    Provalmente vai ter q olhar na API, ver aquilo que o diguin falou, pra pegar uma lista gigante de milhoes de partidas jogadas.
    Todas funcoes tao prontas, so tem q mudar mesmo a lista inicial de jogos, ou seja, basta mudar o gamesDf pra lista nova

2- ajustar alguma coisa pra considerar um numero minimo de 100 partidas por time por ex, porcentagem, algo assim (aquilo q a gente tinha conversado)

3- gerar o arquivo json final com as estatisticas

alem disso provavelmente da pra mudar alguns nomes de variaveis etc pra ficar mais claro se quiser,
e da pra refatorar algumas coisas tbm (deixei alguns comentarios de coisas q podiam ser diferentes), talvez passar pra orientacao a objeto.
Fiz meio de qualquer jeito as funcoes e essa "main", mas deve ter umas partes da main que podem
virar outras funcoes tbm, alem de tirar alguns prints, etc

obs: se vc for rodar em outra pasta, acho q vc vai ter q criar a paste games/ dentro, nao sei se ele cria sozinho

*/
